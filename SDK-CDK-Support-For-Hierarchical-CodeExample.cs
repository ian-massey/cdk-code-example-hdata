/* This document contains programming examples provided by Scribe for illustrative purposes only. Scribe grants
   you a nonexclusive copyright license to use all programming code examples from which you can generate similar
   functionality tailored to your own specific needs.

   These examples have not been thoroughly tested under all conditions and are provided to you "AS IS" without
   any warranties of any kind. Therefore, Scribe cannot guarantee or imply reliability, serviceability, or 
   functionality of these programs. The implied warranties of non-infringement, merchantability, and fitness for
   a particular purpose are expressly disclaimed.
   
   This CSharp code example has been provided to help you understand how you can start leveraging the feature discussed
   under:  
   
   https://success.scribesoft.com/s/article/ka632000000PENNAA4/SDK-CDK-Support-For-Hierarchical-Data.  
   
   Please take a look at this article and supporting documentation to understand the concepts.  
*/

namespace CSharpHierarchicalData
{
    using Scribe.Core.ConnectorApi;
    using Scribe.Core.ConnectorApi.Metadata;
    using Scribe.Core.ConnectorApi.Query;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;

    // Before Hierarchical Data, ObjectDefinitions roughly correspond to user defined classes with simple properties
    public class Contact
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }
    }

    // With Hierarchical Data, the properties can be other user defined ObjectDefinitions
    public class Account
    {
        public string AccountName { get; set; }
        public decimal CreditLimit { get; set; }

        // 1. Object Definitions as Properties
        public Contact MainContact { get; set; }

        // 2. Recursive Object Definitions as Properties
        public Account ParentAccount { get; set; }

        // 3. Properties as Collections, Other Object Definitions
        public IEnumerable<Contact> Contacts { get; set; }

        // 4. Properties as Collections for built in properties
        public IEnumerable<string> Regions { get; set; }
    }

    // Some simplified examples of making the metadata for Account
    public static class HierarchicalMetadata
    {
        private const string QueryName = "Query";
        private const string UpdateName = "Update";
        private const string UpdateWithName = "UpdateWith";

        private const string ContactFullName = "Contact";
        private const string AccountFullName = "Account";

        public static IEnumerable<IActionDefinition> GetActions()
        {
            var query = new ActionDefinition();
            query.FullName = QueryName;
            query.Name = QueryName;
            query.KnownActionType = KnownActions.Query;

            yield return query;

            var update = new ActionDefinition();
            update.FullName = UpdateName;
            update.Name = UpdateName;
            update.SupportsLookupConditions = true;
            update.SupportsInput = true;
            update.KnownActionType = KnownActions.Update;

            yield return update;

            var updateWith = new ActionDefinition();
            updateWith.FullName = UpdateWithName;
            updateWith.Name = UpdateWithName;
            updateWith.SupportsLookupConditions = true;
            updateWith.SupportsInput = true;
            updateWith.KnownActionType = KnownActions.UpdateWith;

            yield return updateWith;
        }
        public static IEnumerable<IObjectDefinition> GetObjects()
        {
            yield return GetContactDef();
            yield return GetHAccountWithContacts();
        }

        private static IObjectDefinition GetContactDef()
        {
            var contactDef = new ObjectDefinition();
            contactDef.FullName = ContactFullName;
            contactDef.Name = ContactFullName;
            contactDef.RelationshipDefinitions = new List<IRelationshipDefinition>();
            contactDef.SupportedActionFullNames = new List<string>();
            contactDef.PropertyDefinitions =
                new List<IPropertyDefinition>
                {
                    MakeProp("FirstName", "System.String", false),
                    MakeProp("LastName", "System.String", false)
                };

            return contactDef;

        }
        private static IObjectDefinition GetHAccountWithContacts()
        {
            var accountWithContactsDef = new ObjectDefinition();
            accountWithContactsDef.FullName = AccountFullName;
            accountWithContactsDef.Name = AccountFullName;
            accountWithContactsDef.RelationshipDefinitions = new List<IRelationshipDefinition>();
            accountWithContactsDef.SupportedActionFullNames = new List<string> { QueryName, UpdateName, UpdateWithName };

            accountWithContactsDef.PropertyDefinitions =
                new List<IPropertyDefinition>
                {
                    MakeProp("AccountName", "System.String", false),
                    MakeProp("CreditLimit", "System.Decimal", false),
                    MakeProp("MainContact", ContactFullName, false),
                    MakeProp("ParentAccount", AccountFullName, false),
                    MakeProp("Contacts", ContactFullName, true),
                    MakeProp("Regions", "System.String", true),
                };

            return accountWithContactsDef;

        }

        private static IPropertyDefinition MakeProp(string name, string propType, bool isCollection)
        {
            var prop = new PropertyDefinition();
            prop.FullName = name;
            prop.Name = name;
            prop.PropertyType = propType;
            prop.MaxOccurs = isCollection ? -1 : 1;

            // Other properties should be set, UsedIn..., but they are orthogonal to Hierarchical Data
            prop.UsedInActionInput = true;
            prop.UsedInActionOutput = true;
            prop.UsedInLookupCondition = true;
            prop.UsedInQueryConstraint = true;
            prop.UsedInQuerySelect = true;
            prop.UsedInQuerySequence = true;

            return prop;
        }
    }

    public static class Samples
    {
        public static Account CreateSampleAcct()
        {
            // I am going to build up the Account data here for simplicity
            var account = new Account();
            account.AccountName = "Acme, Inc.";
            account.CreditLimit = 1000.00M;

            var parentAccount = new Account();
            parentAccount.AccountName = "Warner Bros.";
            parentAccount.CreditLimit = 1001.00M;

            // For brevity, keeping the other parent accounts defaulted to null
            account.ParentAccount = parentAccount;

            var contact1 = new Contact();
            contact1.FirstName = "Bugs";
            contact1.LastName = "Bunny";

            account.MainContact = contact1;

            var contact2 = new Contact();
            contact2.FirstName = "Daffy";
            contact2.LastName = "Duck";

            account.Contacts = new[] { contact1, contact2 };

            account.Regions = new[] { "NE", "SE" };
            return account;
        }

    }
    public static class Converter
    {
        public static DataEntity ConvertAccountToDataEntity(Account account)
        {
            var accountAsDataEntity = new DataEntity();
            accountAsDataEntity.ObjectDefinitionFullName = "Account";

            // Just like before, the simple properties go in the properties
            var accountProperties = new EntityProperties();
            accountProperties.Add("AccountName", account.AccountName);
            accountProperties.Add("CreditLimit", account.CreditLimit);

            // Taking the remaining four properties in the order in which they are defined above

            // Contact requires us to place the property in the same place that it would be for Relationships
            var contact1AsDataEntity = new DataEntity("Contact");
            var contactProperties = new EntityProperties();
            contactProperties.Add("FirstName", account.MainContact.FirstName);
            contactProperties.Add("LastName", account.MainContact.LastName);
            contact1AsDataEntity.Properties = contactProperties;

            var accountChildren = new EntityChildren();
            accountChildren.Add("MainContact", new List<DataEntity> { contact1AsDataEntity });

            var parentAccountAsDataEntity = new DataEntity("Account");
            var parentAccountProperties =
                new EntityProperties
                {
                    { "AccountName", account.ParentAccount.AccountName },
                    { "CreditLimit", account.ParentAccount.CreditLimit }
                };

            parentAccountAsDataEntity.Properties = parentAccountProperties;

            accountChildren.Add("ParentAccount", new List<DataEntity> { parentAccountAsDataEntity });

            // Conveniently, for collections of other Data Entities, we already have a list to add to,
            // but typically you will not know how long the list is, so you are better off defining a function
            // that can convert for you. I will use a ContactToDataEntity function below for the list example (and in
            // a real world scenario you would refactor the above to also you use that function).
            accountChildren.Add(
                "Contacts",
                account.Contacts
                    .Select(ContactToDataEntity)
                    .ToList());

            // For collections of instances non-user defined ObjectDefinitons (e.g. strings, ints, decimals, etc.)
            // These still go in the property collection. The type of collections of simple types should be List<T>
            accountProperties.Add("Regions", account.Regions.ToList());

            accountAsDataEntity.Properties = accountProperties;
            accountAsDataEntity.Children = accountChildren;

            return accountAsDataEntity;
        }

        public static DataEntity ContactToDataEntity(Contact contact)
        {
            var dataEntity = new DataEntity("Contact");
            var contactProperties = new EntityProperties();
            contactProperties.Add("FirstName", contact.FirstName);
            contactProperties.Add("LastName", contact.LastName);
            dataEntity.Properties = contactProperties;
            return dataEntity;
        }

        public static Account ConvertDataEntityToHAccountWithContacts(DataEntity d)
        {
            if (d == null) return null;
            Debug.Assert(d.ObjectDefinitionFullName == "Account");

            var account = new Account();
            if (d.Properties != null)
            {
                if (d.Properties.TryGetValue("AccountName", out var accountName)) // Notice I am using the new inline var declaration
                {
                    account.AccountName = accountName.ToString();
                }

                if (d.Properties.TryGetValue("CreditLimit", out var creditLimit))
                {
                    account.CreditLimit = (decimal)creditLimit;
                }

                if (d.Properties.TryGetValue("Regions", out var regions))
                {
                    account.Regions = (List<string>)regions; // These are expected to be of type List<T>.
                }
            }

            if (d.Children != null)
            {
                if (d.Children.TryGetValue("ParentAccount", out var parentAccountList)) // Notice I am using the new inline var declaration
                {
                    var parent = parentAccountList.FirstOrDefault();

                    if (parent != null)
                    {
                        account.ParentAccount = ConvertDataEntityToHAccountWithContacts(parent); // Notice the recursion
                    }
                }

                if (d.Children.TryGetValue("MainContact", out var mainContactList)) // Notice I am using the new inline var declaration
                {
                    if (mainContactList != null)
                    {
                        var mainContact = mainContactList.FirstOrDefault();

                        if (mainContact != null)
                        {
                            account.MainContact = ConvertDataEntityToContact(mainContact); // Notice the recursion
                        }
                    }
                }

                if (d.Children.TryGetValue("Contacts", out var contacts)) // Notice I am using the new inline var declaration
                {
                    if (contacts != null)
                    {
                        account.Contacts = contacts.Select(ConvertDataEntityToContact).ToList();
                    }
                }
            }

            return account;
        }

        public static Contact ConvertDataEntityToContact(DataEntity d)
        {
            if (d == null) return null;
            Debug.Assert(d.ObjectDefinitionFullName == "Contact");

            var contact = new Contact();
            if (d.Properties != null)
            {
                if (d.Properties.TryGetValue("FirstName", out var fname))
                {
                    contact.FirstName = fname.ToString();
                }

                if (d.Properties.TryGetValue("LastName", out var lname))
                {
                    contact.LastName = lname.ToString();
                }
            }

            return contact;
        }
    }
}


